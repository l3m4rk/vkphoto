package test.task.vkphoto.models.auth

import com.google.gson.annotations.SerializedName

data class AuthResponse(
        @SerializedName("access_token") val token: String,
        @SerializedName("user_id") val userId: Long
)