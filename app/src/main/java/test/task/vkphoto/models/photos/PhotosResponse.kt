package test.task.vkphoto.models.photos

import com.google.gson.annotations.SerializedName

data class PhotosResponse(
        @SerializedName("response") val photos: List<PhotoRaw>
)