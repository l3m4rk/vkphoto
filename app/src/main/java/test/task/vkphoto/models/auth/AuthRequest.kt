package test.task.vkphoto.models.auth

import com.google.gson.annotations.SerializedName

data class AuthRequest(
        @SerializedName("grant_type") val grantType: String = "password",
        @SerializedName("client_id") val clientId: String = Constants.CLIENT_ID,
        @SerializedName("client_secret") val clientSecret: String = Constants.CLIENT_SECRET,
        @SerializedName("username") val userName: String,
        @SerializedName("password") val password: String
)