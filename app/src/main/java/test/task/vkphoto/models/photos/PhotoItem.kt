package test.task.vkphoto.models.photos

data class PhotoItem(val id: String, val photoUrl: String)