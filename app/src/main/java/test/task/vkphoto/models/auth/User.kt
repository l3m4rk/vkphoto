package test.task.vkphoto.models.auth

data class User(val token: String, val id: Long)