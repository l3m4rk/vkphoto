package test.task.vkphoto.models.photos

import com.google.gson.annotations.SerializedName

data class PhotoRaw(
        @SerializedName("pid") val pid: Long,
        @SerializedName("aid") val aid: Long,
        @SerializedName("owner_id") val ownerId: Long,
        @SerializedName("post_id") val postId: Long,
        @SerializedName("src") val photo: String,
        @SerializedName("src_big") val bigPhoto: String,
        @SerializedName("src_xbig") val xbigPhoto: String,
        @SerializedName("src_small") val smallPhoto: String,
        @SerializedName("text") val text: String,
        @SerializedName("created") val created: Long
)