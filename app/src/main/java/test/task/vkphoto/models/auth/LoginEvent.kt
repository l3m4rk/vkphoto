package test.task.vkphoto.models.auth

data class LoginEvent(val login: String, val password: String)