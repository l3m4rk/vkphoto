package test.task.vkphoto

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import test.task.vkphoto.di.DaggerAppComponent
import timber.log.Timber

class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<App> {
        return DaggerAppComponent.builder()
                .application(this)
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}