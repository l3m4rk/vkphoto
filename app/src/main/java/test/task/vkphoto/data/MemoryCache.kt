package test.task.vkphoto.data

import android.graphics.Bitmap
import android.support.v4.util.LruCache
import test.task.vkphoto.models.photos.PhotoItem
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MemoryCache @Inject constructor() {

    private val cacheSize = (Runtime.getRuntime().maxMemory() / 1024).toInt() / 8

    private val memoryCache = object : LruCache<String, Bitmap>(cacheSize) {

        override fun sizeOf(key: String?, bitmap: Bitmap): Int {
            return bitmap.byteCount / 1024
        }
    }

    fun addBitmapToMemoryCache(key: String, bitmap: Bitmap) {
        if (memoryCache[key] == null) {
            memoryCache.put(key, bitmap)
        }
    }

    fun getBitmapFromCache(key: String): Bitmap? = memoryCache[key]

    fun isEmpty() = memoryCache.size() == 0

    fun size(): Int {
        return memoryCache.size()
    }

    var photos: List<PhotoItem>? = null

}