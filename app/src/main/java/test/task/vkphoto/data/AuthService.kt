package test.task.vkphoto.data

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import test.task.vkphoto.models.auth.AuthResponse
import test.task.vkphoto.models.auth.Constants

interface AuthService {

    @GET("token")
    fun getToken(
            @Query("grant_type") grantType: String = "password",
            @Query("client_id") clientId: String = Constants.CLIENT_ID,
            @Query("client_secret") clientSecret: String = Constants.CLIENT_SECRET,
            @Query("username") userName: String,
            @Query("password") password: String
    ): Call<AuthResponse>

}

