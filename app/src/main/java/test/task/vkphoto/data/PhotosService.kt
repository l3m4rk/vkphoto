package test.task.vkphoto.data

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url
import test.task.vkphoto.models.photos.PhotosResponse

interface PhotosService {

    @GET("method/photos.get")
    fun getPhotos(
            @Query("owner_id") ownerId: Long,
            @Query("access_token") token: String,
            @Query("album_id") albumId: String = "wall",
            @Query("count") count: Int = 30,
            @Query("offset") offset: Int = 0,
            @Query("rev") order: Int = 1,    // reverse chronological order
            @Query("extended") extended: Int = 1
    ): Call<PhotosResponse>

    @GET("method/photos.getAll")
    fun getAllPhotos(
            @Query("owner_id") ownerId: Long,
            @Query("access_token") token: String,
            @Query("count") count: Int = 30,
            @Query("offset") offset: Int = 0,
            @Query("rev") order: Int = 1,    // reverse chronological order
            @Query("extended") extended: Int = 1,
            @Query("skip_hidden") skipHidden: Int = 1
    ): Call<PhotosResponse>

    @GET
    fun loadPhoto(@Url url: String): Call<ResponseBody>


}

