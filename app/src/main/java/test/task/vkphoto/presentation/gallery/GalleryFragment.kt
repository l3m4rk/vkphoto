package test.task.vkphoto.presentation.gallery

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import dagger.android.support.DaggerFragment
import test.task.vkphoto.R
import test.task.vkphoto.data.MemoryCache
import test.task.vkphoto.presentation.MainActivity
import javax.inject.Inject

class GalleryFragment : DaggerFragment() {

    @Inject lateinit var memoryCache: MemoryCache
    @Inject lateinit var galleryAdapter: GalleryAdapter
    private var position: Int? = null

    companion object {
        val TAG = GalleryFragment::class.java.simpleName
        const val ARG_POSITION = "position"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        if (arguments != null) {
            position = arguments?.getInt(ARG_POSITION)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)
        setupViewPager(root)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        updateSubtitle()
    }

    private fun updateSubtitle() {
        (activity as MainActivity).supportActionBar?.subtitle = "${position?.plus(1)} of ${memoryCache.photos?.size}"
    }

    private fun setupViewPager(root: View) {
        val viewPager = root.findViewById<ViewPager>(R.id.viewPager)
        viewPager.adapter = galleryAdapter
        viewPager.setCurrentItem(position!!, false)
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                (activity as MainActivity).supportActionBar?.subtitle = "${position + 1} of ${memoryCache.photos?.size}"
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                (activity as MainActivity).hideGallery()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStart() {
        super.onStart()
        setupTitle()
    }

    private fun setupTitle() {
        (activity as MainActivity).supportActionBar?.title = "Gallery"
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

}

