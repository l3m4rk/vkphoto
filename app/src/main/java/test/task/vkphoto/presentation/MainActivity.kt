package test.task.vkphoto.presentation

import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity
import test.task.vkphoto.R
import test.task.vkphoto.presentation.gallery.GalleryFragment
import test.task.vkphoto.presentation.login.LoginFragment
import test.task.vkphoto.presentation.photos.PhotosFragment
import test.task.vkphoto.repositories.UserRepository
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject lateinit var userRepository: UserRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (userRepository.isLoggedIn()) {
            if (supportFragmentManager.backStackEntryCount > 0) {
                return
            } else {
                showPhotos()
            }
        } else {
            showLogin()
        }
    }

    private fun showLogin() {
        supportActionBar?.title = "Login"
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, LoginFragment())
                .commit()
    }

    fun showPhotos() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, PhotosFragment())
                .commit()
    }

    fun showGallery(position: Int) {
        val galleryFragment = GalleryFragment()
        galleryFragment.arguments = Bundle().apply { putInt(GalleryFragment.ARG_POSITION, position) }
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, galleryFragment)
                .addToBackStack(GalleryFragment.TAG)
                .commit()
    }

    fun hideGallery() {
        supportFragmentManager.popBackStack()
    }
}
