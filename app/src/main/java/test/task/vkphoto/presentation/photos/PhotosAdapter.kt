package test.task.vkphoto.presentation.photos

import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import test.task.vkphoto.R
import test.task.vkphoto.data.MemoryCache
import test.task.vkphoto.data.PhotosService
import test.task.vkphoto.domain.photos.ImageLoadTask
import test.task.vkphoto.models.photos.PhotoItem
import java.util.*
import java.util.concurrent.Executors
import javax.inject.Inject

class PhotosAdapter
@Inject constructor(
        private val photosService: PhotosService,
        private val cache: MemoryCache
) : RecyclerView.Adapter<PhotosAdapter.PhotoHolder>() {

    companion object {
        @JvmStatic private val THREADS = 12
    }

    private val photos = ArrayList<PhotoItem>()
    private val executor = Executors.newFixedThreadPool(THREADS)

    var listener: ItemClickListener? = null

    interface ItemClickListener {
        fun onItemClick(position: Int)
    }


    fun update(photos: List<PhotoItem>) {
        this.photos.clear()
        this.photos.addAll(photos)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.item_photo, parent, false)
        return PhotoHolder(itemView)
    }

    override fun getItemCount(): Int {
        return photos.size
    }

    override fun onBindViewHolder(holder: PhotoHolder, position: Int) {
        val item = photos[position]

        holder.itemView.setOnClickListener { listener?.onItemClick(position) }

        val bitmap = cache.getBitmapFromCache(item.id)
        if (bitmap == null) {
            ImageLoadTask(cache, photosService, holder.photo).executeOnExecutor(executor, item)
        } else {
            holder.photo.setImageBitmap(bitmap)
        }
    }


    class PhotoHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            setIsRecyclable(false)
        }

        val photo: AppCompatImageView = itemView.findViewById(R.id.photo)

    }

}