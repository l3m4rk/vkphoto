package test.task.vkphoto.presentation.login

import android.os.Bundle
import android.support.v7.widget.AppCompatEditText
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import dagger.android.support.DaggerFragment
import test.task.vkphoto.R
import test.task.vkphoto.data.AuthService
import test.task.vkphoto.domain.LoadListener
import test.task.vkphoto.domain.login.LoginTask
import test.task.vkphoto.models.auth.AuthResponse
import test.task.vkphoto.models.auth.LoginEvent
import test.task.vkphoto.models.auth.User
import test.task.vkphoto.presentation.MainActivity
import test.task.vkphoto.repositories.UserRepository
import timber.log.Timber
import java.util.concurrent.Executor
import javax.inject.Inject

class LoginFragment : DaggerFragment(), LoadListener<AuthResponse> {
    override fun onLoad(result: AuthResponse) {
        Timber.i("Result is $result")

        (activity as MainActivity).showPhotos()

        val user = User(result.token, result.userId)
        userRepository.saveUser(user)
    }

    override fun onFailure(throwable: Throwable) {
        Log.w("Error", throwable)
    }

    @Inject lateinit var authService: AuthService
    @Inject lateinit var executor: Executor
    @Inject lateinit var userRepository: UserRepository

    private lateinit var username: AppCompatEditText
    private lateinit var password: AppCompatEditText

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        username = view.findViewById(R.id.username)
        password = view.findViewById(R.id.password)

        view.findViewById<Button>(R.id.loginButton).setOnClickListener {
            login(obtainLoginEvent())
        }

        return view
    }

    private fun obtainLoginEvent() =
            LoginEvent(
                    username.text.toString().trim(),
                    password.text.toString().trim()
            )

    private fun login(loginEvent: LoginEvent) {
        val loginTask = LoginTask(authService, loginEvent, this)
        executor.execute(loginTask)
    }


}


