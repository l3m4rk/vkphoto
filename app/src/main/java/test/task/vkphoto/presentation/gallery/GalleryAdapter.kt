package test.task.vkphoto.presentation.gallery

import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import test.task.vkphoto.R
import test.task.vkphoto.data.MemoryCache
import javax.inject.Inject

class GalleryAdapter
@Inject constructor(private val memoryCache: MemoryCache) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageView = LayoutInflater.from(container.context).inflate(R.layout.item_gallery, container, false) as ImageView
        val key = memoryCache.photos!![position].id
        imageView.setImageBitmap(memoryCache.getBitmapFromCache(key))
        container.addView(imageView)
        return imageView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int = memoryCache.photos!!.size

}