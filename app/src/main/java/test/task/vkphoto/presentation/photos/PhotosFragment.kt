package test.task.vkphoto.presentation.photos

import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import dagger.android.support.DaggerFragment
import test.task.vkphoto.R
import test.task.vkphoto.data.MemoryCache
import test.task.vkphoto.data.PhotosService
import test.task.vkphoto.domain.photos.GetPageTask
import test.task.vkphoto.models.auth.User
import test.task.vkphoto.models.photos.PhotoItem
import test.task.vkphoto.presentation.MainActivity
import test.task.vkphoto.domain.LoadListener
import timber.log.Timber
import java.util.concurrent.Executors
import javax.inject.Inject

class PhotosFragment : DaggerFragment(), LoadListener<List<PhotoItem>>, PhotosAdapter.ItemClickListener {

    private val handler = Handler()

    override fun onItemClick(position: Int) {
        (activity as MainActivity).showGallery(position)
    }

    override fun onLoad(result: List<PhotoItem>) {
        handler.post {
            progressBar.visibility = View.GONE
            adapter.update(result)
            memoryCache.photos = result
        }
    }

    override fun onFailure(throwable: Throwable) {
        Timber.w(throwable)
        handler.post { progressBar.visibility = View.GONE }
    }

    @Inject lateinit var adapter: PhotosAdapter
    @Inject lateinit var photosService: PhotosService
    @Inject lateinit var memoryCache: MemoryCache

    private lateinit var progressBar: ProgressBar

    private val executor = Executors.newSingleThreadExecutor()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_photos, container, false)
        setupList(view)
        return view
    }

    private fun setupList(view: View) {
        progressBar = view.findViewById(R.id.progress)
        val photosList = view.findViewById<RecyclerView>(R.id.photosList)
        photosList.itemAnimator = DefaultItemAnimator()

        photosList.layoutManager = GridLayoutManager(context, spanCount())

        photosList.setHasFixedSize(true)

        adapter.listener = this

        photosList.adapter = adapter
    }

    private fun spanCount(): Int {
        return if (activity?.resources?.configuration?.orientation == Configuration.ORIENTATION_PORTRAIT) 2 else 4
    }

    override fun onStart() {
        super.onStart()
        setupTitle()
        if (memoryCache.isEmpty())
            loadPhotos()
        else
            updatePhotos()
    }

    private fun setupTitle() {
        (activity as MainActivity).supportActionBar?.title = "Photos"
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        (activity as MainActivity).supportActionBar?.subtitle = null
    }

    private fun loadPhotos() {
        val token = PreferenceManager.getDefaultSharedPreferences(context).getString("TOKEN", null)
        val id = PreferenceManager.getDefaultSharedPreferences(context).getLong("ID", -1)
        val user = User(token, id)

        progressBar.visibility = View.VISIBLE
        val getPageTask = GetPageTask(photosService, user, this)
        executor.execute(getPageTask)
    }

    private fun updatePhotos() {
        adapter.update(memoryCache.photos!!)
    }

    override fun onStop() {
        super.onStop()
//        executor.shutdownNow()
    }

}

