package test.task.vkphoto.domain.photos

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.os.AsyncTask
import android.support.v7.widget.AppCompatImageView
import android.widget.ImageView
import test.task.vkphoto.data.MemoryCache
import test.task.vkphoto.data.PhotosService
import test.task.vkphoto.models.photos.PhotoItem
import java.lang.ref.WeakReference

class ImageLoadTask(
        private val cache: MemoryCache,
        private val photosService: PhotosService,
        photo: AppCompatImageView
) : AsyncTask<PhotoItem, Void, Bitmap>() {

    private val imageView = WeakReference<ImageView>(photo)

    override fun doInBackground(vararg params: PhotoItem?): Bitmap? {
        val photo = params[0]!!
        val responseBody = photosService.loadPhoto(photo.photoUrl).execute()
        return if (responseBody.isSuccessful) {
            val bitmap = BitmapFactory.decodeStream(responseBody.body()?.byteStream())
            cache.addBitmapToMemoryCache(photo.id, bitmap)
//            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).absoluteFile
//            val file = File(imageView.get()?.context?.cacheDir?.toString() + File.pathSeparator + photo.id)
//            file.createNewFile()

//                bitmap
            val view = imageView.get()
            if (view != null) {
                val dimension = Math.min(bitmap.width, bitmap.height)
                ThumbnailUtils.extractThumbnail(bitmap, dimension, dimension)
//                    if (bitmap.width > bitmap.height) {
//                        Bitmap.createBitmap(bitmap, bitmap.width / 2 - bitmap.height / 2, 0, bitmap.height, bitmap.height)
//                    } else {
//                        Bitmap.createBitmap(bitmap, 0, bitmap.height / 2 - bitmap.width / 2, bitmap.width, bitmap.width)
//                    }
            } else {
//                    Bitmap.createScaledBitmap(bitmap, 400, 400, true)
                bitmap
            }
        } else {
            null
        }
    }

    override fun onPostExecute(result: Bitmap?) {
        if (result != null) {
            val photoView = imageView.get()
            photoView?.setImageBitmap(result)
        }
    }
}