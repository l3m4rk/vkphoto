package test.task.vkphoto.domain

interface LoadListener<T> {
    fun onLoad(result: T)

    fun onFailure(throwable: Throwable)
}