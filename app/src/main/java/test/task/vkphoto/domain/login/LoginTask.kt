package test.task.vkphoto.domain.login

import test.task.vkphoto.data.AuthService
import test.task.vkphoto.models.auth.AuthResponse
import test.task.vkphoto.models.auth.LoginEvent
import test.task.vkphoto.domain.LoadListener

class LoginTask(
        private val authService: AuthService,
        private val loginEvent: LoginEvent,
        private val loadListener: LoadListener<AuthResponse>
) : Runnable {

    override fun run() {
        try {
            val response = authService.getToken(userName = loginEvent.login, password = loginEvent.password).execute()

            if (response.isSuccessful) {
                loadListener.onLoad(response.body()!!)
            } else {
                loadListener.onFailure(Throwable("Error"))
            }
        } catch (t: Throwable) {
            loadListener.onFailure(t)
        }
    }

}