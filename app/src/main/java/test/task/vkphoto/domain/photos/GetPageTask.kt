package test.task.vkphoto.domain.photos

import test.task.vkphoto.data.PhotosService
import test.task.vkphoto.models.photos.PhotoItem
import test.task.vkphoto.domain.LoadListener
import test.task.vkphoto.models.auth.User

class GetPageTask(
        private val photosService: PhotosService,
        private val user: User,
        private val listener: LoadListener<List<PhotoItem>>
) : Runnable {

    override fun run() {
        try {
            val response = photosService.getPhotos(user.id, user.token).execute()
            if (response.isSuccessful) {
                val result = response.body()?.photos?.map { PhotoItem(it.pid.toString(), it.bigPhoto) }
                if (result == null) {
                    listener.onFailure(Throwable("Error wrong response format"))
                } else {
                    listener.onLoad(result)
                }
            } else {
                listener.onFailure(Throwable("Error with code ${response.code()}"))
            }
        } catch (ex: InterruptedException) {
            listener.onFailure(Throwable("Task has been interrupted"))
        }
    }

}