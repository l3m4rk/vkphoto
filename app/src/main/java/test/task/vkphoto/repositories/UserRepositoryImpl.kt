package test.task.vkphoto.repositories

import android.content.SharedPreferences
import test.task.vkphoto.models.auth.User
import javax.inject.Inject

class UserRepositoryImpl
@Inject constructor(private val preferences: SharedPreferences) : UserRepository {


    override fun saveUser(user: User) {
        preferences.edit().putString("TOKEN", user.token).apply()
        preferences.edit().putLong("ID", user.id).apply()
    }

    override fun isLoggedIn(): Boolean {
        return preferences.contains("TOKEN")
    }

    override fun getUser(): User? {
        val token = preferences.getString("TOKEN", null)
        val id = preferences.getLong("ID", -1)
        return if (token != null && id != -1L) {
            User(token, id)
        } else {
            null
        }

    }
}