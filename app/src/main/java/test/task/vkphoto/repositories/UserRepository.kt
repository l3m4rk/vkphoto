package test.task.vkphoto.repositories

import test.task.vkphoto.models.auth.User

interface UserRepository {

    fun saveUser(user: User)
    fun getUser(): User?
    fun isLoggedIn(): Boolean

}