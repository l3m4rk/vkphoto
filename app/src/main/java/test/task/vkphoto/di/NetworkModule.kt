package test.task.vkphoto.di

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import test.task.vkphoto.data.AuthService
import test.task.vkphoto.data.PhotosService
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    companion object {
        @JvmStatic private val authUrl = "https://oauth.vk.com/"
        @JvmStatic private val photosUrl = "https://api.vk.com/"
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val log = HttpLoggingInterceptor()
        log.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
                .addNetworkInterceptor(log)
                .build()
    }

    @Provides
    @Singleton
    @Named("auth")
    fun provideAuthClient(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl(authUrl)
                .build()
    }

    @Provides
    @Singleton
    @Named("photos")
    fun providePhotosClient(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl(photosUrl)
                .build()
    }


    @Provides
    @Singleton
    fun provideAuthService(@Named("auth") retrofit: Retrofit): AuthService {
        return retrofit.create(AuthService::class.java)
    }

    @Provides
    @Singleton
    fun providePhotosService(@Named("photos") retrofit: Retrofit): PhotosService {
        return retrofit.create(PhotosService::class.java)
    }

}