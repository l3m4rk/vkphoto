package test.task.vkphoto.di

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import test.task.vkphoto.App
import test.task.vkphoto.repositories.UserRepository
import test.task.vkphoto.repositories.UserRepositoryImpl
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: App): Context {
        return app.applicationContext
    }

    @Provides
    @Singleton
    fun provideExecutor(): Executor {
        return Executors.newSingleThreadExecutor()
    }

    @Provides
    @Singleton
    fun providePreferences(context: Context) =
            PreferenceManager.getDefaultSharedPreferences(context)

    @Provides
    @Singleton
    fun provideUserRepository(preferences: SharedPreferences): UserRepository {
        return UserRepositoryImpl(preferences)
    }


}