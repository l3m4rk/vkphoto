package test.task.vkphoto.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import test.task.vkphoto.presentation.MainActivity
import test.task.vkphoto.presentation.gallery.GalleryFragment
import test.task.vkphoto.presentation.login.LoginFragment
import test.task.vkphoto.presentation.photos.PhotosFragment

@Module
abstract class AndroidBindingModule {

    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun loginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun photosFragment(): PhotosFragment

    @ContributesAndroidInjector
    abstract fun galleryFragment(): GalleryFragment

}